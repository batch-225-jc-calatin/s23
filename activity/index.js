






let pokemonGo = {
    name: 'Ash Kentchum', 
    age: 10,
    pokemon: ['pickachu', 'charizard', 'squirtle','bulbasaur'],
    friend:{
        hoen:['May', 'Max'],
        kanto:['Brock', 'Misty'],
    }
}
console.log(pokemonGo);
console.log('Result of dot notation: ' )
console.log(pokemonGo.name)
console.log('Result of square bracket notation');
console.log(pokemonGo['pokemon']);


let call = {
    pokemon: 'pickachu',
    talk:function(){
        console.log(this.pokemon + ' ' + 'I choose You!')
    }
}
console.log('Result of talk method');
call.talk();



// =================GAME=============

console.log('Pokemon Game');




function Pokemon(name, level, health, attack) {

    // Properties
    this.name = name;
    this.level = level;
    this.health = health
    this.attack = attack;
    this.health = 2 * level;
    this.attack = level;

    //Methods
    this.tackle = function(target) {
        console.log(this.name + ' tackled ' + target.name);
        target.health -= this.attack;
        console.log( target.name + "'s health is now reduced to " + target.health);
        };
    this.faint = function(){
        console.log(this.name + ' fainted.');
    }

}

// Creates new instances of the "Pokemon" object each with their unique properties
let pikachu = new Pokemon("Pikachu", 12, 24, 12);
let geodude = new Pokemon('Geodude', 8, 16, 8);
let mewtwo = new Pokemon('Mewtwo', 100, 200, 100);
// Providing the "rattata" object as an argument to the "pikachu" tackle method will create interaction between the two objects
console.log(pikachu);
console.log(geodude);
console.log(mewtwo);

geodude.tackle(pikachu) ;
console.log(pikachu);

mewtwo.tackle(geodude);
geodude.faint(geodude);
console.log(geodude);


